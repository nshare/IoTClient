﻿namespace IoTClient.Models
{
    public class MitsubishiA1Data
    {
        /// <summary>
        /// 开始地址
        /// </summary>
        public int BeginAddress { get; set; }
        /// <summary>
        /// 数据类型
        /// </summary>
        public MitsubishiA1Type MitsubishiA1Type { get; set; }
    }
}
